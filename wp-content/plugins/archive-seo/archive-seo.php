<?php
/*
Plugin Name: Archive SEO Optimizer
Plugin URI: http://fastcoding.jp/
Description: 一般的なSEO対策プラグインでサポートされない投稿アーカイブページにおいて、タイトル・メタディスクリプション・メタキーワードを設定可能にする。All in one SEOが導入前提プラグインです。
Author: Fastcoding Mr.K
Version: 0.1
Author URI: http://fastcoding.jp/
*/

add_action("admin_menu", "archive_seo");

function archive_seo(){
    add_options_page('Archive SEO', 'Archive SEO', 8, 'archive_seo', 'archive_seo_page');
    
    //Add option
    add_option("fc_archive_seo_settings", $value="", $depricated="", $autoload='yes');
}

function archive_seo_settings_apply(){
    $post_types = $_POST['post_types'];
    if(!empty($post_types)){
        $post_types = explode(',', $post_types);
        
        $option_value = array();
        foreach($post_types as $post_type_name){
            $dataset = array(
                'title' => $_POST['title_' . $post_type_name],
                'description' => $_POST['description_' . $post_type_name],
                'keyword' => $_POST['keyword_' . $post_type_name],
            );
            $option_value[$post_type_name] = $dataset;
           
        }
        
        #GIang update for taxonomy 
         $seo_taxonomy = $_POST['seo_taxonomy'];
         
        foreach($seo_taxonomy as $_key){
          $tax_dataset = array(
              'title' => $_POST['tax_title_' . $_key],
              'description' => $_POST['tax_description_' . $_key],
              'keyword' => $_POST['tax_keyword_' . $_key],
          );
          $option_value['tax'][$_key] = $tax_dataset;
        }
        #END GIang update for taxonomy 
    }
    if(!empty($option_value)){
        update_option("fc_archive_seo_settings", $option_value);
    }
}

//hook for AIOSEO
function archive_seo_title_display($title){
    $settings = get_option("fc_archive_seo_settings");
    if(!empty($settings)){
        $is_cpt_archive = false;
        
        foreach($settings as $key => $value){
            if(is_post_type_archive($key) && !is_search()){
                $is_cpt_archive = true;
                if($value["title"] != ""){
                    $title = $value["title"];
                    $title = str_replace('%blog_title%',get_bloginfo('name'),$title);
                }
            }
        }
        
        //for post
        if(is_archive() AND !$is_cpt_archive AND !is_search()){
            if($settings["post"]["title"] != ""){
                $title = $settings["post"]["title"];
                $title = str_replace('%blog_title%',get_bloginfo('name'),$title);
            }
        }
        
        //Giang update title for taxonomy
        if(is_tax()){
          foreach($settings['tax'] as $_key => $_value){
            if(is_tax($_key)){
              $title = $settings["tax"][$_key]["title"];
              $queried_object = get_queried_object();
              $title = str_replace('%taxonomy_name%',$queried_object->name,$title);
              $title = str_replace('%blog_title%',get_bloginfo('name'),$title);
              
            }
          }
        }
        //END : Giang update title for taxonomy
    }
    
    return $title;
}
add_filter('aioseop_title', 'archive_seo_title_display');

//hook for wp_head()
function archive_seo_meta_display(){
    $settings = get_option("fc_archive_seo_settings");
    if(!empty($settings)){
        $is_cpt_archive = false;
        
        foreach($settings as $key => $value){
            if(is_post_type_archive($key) AND !is_tax()){
                $is_cpt_archive = true;
                if(!empty($value["description"]) OR !empty($value["keyword"])){
                    echo '<!-- Archive SEO -->'.PHP_EOL;
                }
                
                if(!empty($value["description"])){
                    echo '<meta name="description" content="'.$value["description"].'">'.PHP_EOL;
                }
                if(!empty($value["keyword"])){
                    echo '<meta name="keywords" content="'.$value["keyword"].'">'.PHP_EOL;
                }
            }
        }
        
        //for post
        if(is_archive() AND !$is_cpt_archive AND !is_tax()){
            if(!empty($settings["post"]["description"]) OR !empty($settings["post"]["keyword"])){
                echo '<!-- Archive SEO -->'.PHP_EOL;
            }

            if(!empty($settings["post"]["description"])){
                echo '<meta name="description" content="'.$settings["post"]["description"].'">'.PHP_EOL;
            }
            if(!empty($settings["post"]["keyword"])){
                echo '<meta name="keywords" content="'.$settings["post"]["keyword"].'">'.PHP_EOL;
            }
        }
        
        
        //Giang update meta for taxonomy
        if(is_tax()){
          $_taxs = $settings['tax'] ? $settings['tax'] : array();
          foreach($_taxs as $_key => $_value){
            if(is_tax($_key)){
              if(!empty($_value["description"]) OR !empty($_value["keyword"])){
                echo '<!-- Archive SEO -->'.PHP_EOL;
              }
              
              if(!empty($_value["description"])){
                echo '<meta name="description" content="'.$_value["description"].'">'.PHP_EOL;
              }
              if(!empty($_value["keyword"])){
                echo '<meta name="keywords" content="'.$_value["keyword"].'">'.PHP_EOL;
              }
            }
          }
        }
        //END : Giang update meta for taxonomy
    }
}
add_action('wp_head', 'archive_seo_meta_display');

//css including
function archive_seo_style(){
    wp_enqueue_style( 'archive_seo_style', plugins_url().'/archive-seo/style.css' );
}
add_action( 'admin_enqueue_scripts', 'archive_seo_style' );

//function
function get_post_types_fc() {
    $post_type = get_post_types( array( '_builtin' => false, 'publicly_queryable' => true, 'show_ui' => true ) );
    return $post_type;
}
//update for taxonomy
function get_taxonomy_fc() {
  $post_type = get_post_types( array( '_builtin' => false, 'publicly_queryable' => true, 'show_ui' => true ) );
  return $post_type;
}

function archive_seo_page(){
    $post_types = get_post_types_fc();
    
    //Make comma divided list of post type
    $types_list = "";
    foreach($post_types as $a_posttype){
        $types_list .= $a_posttype . ",";
    }
    $types_list = substr($types_list, 0, -1);
    
    archive_seo_settings_apply();
    
    $seo_data = get_option("fc_archive_seo_settings");
?>

<h2>Archive SEO Optimizer</h2>
<div class="wrap archive-seo">
    
    <pre><?php //var_dump($post_types); ?></pre>
    <div class="postbox">
        <p>投稿タイプのアーカイブページのSEOができます。</p>
        <p>それぞれの投稿タイプのSEOタグを入力してください。</p>
        <p>もし入力されない場合はAll in one SEOの設定が優先されます。</p>
    </div>
    
    <form method="post" action="">
        <input type="hidden" name="post_types" value="post,<?php echo $types_list ?>">
        <h3>Settings for Default Post(投稿)</h3>
        <table>
            <tbody>
                <tr>
                    <th>
                        <label>Title</label>
                    </th>
                    <td>
                        <input type="text" name="title_post" value="<?php echo @$seo_data["post"]["title"] ?>">
                    </td>
                </tr>
                <tr>
                    <th>
                        <label>Description</label>
                    </th>
                    <td>
                        <textarea name="description_post"><?php echo @$seo_data["post"]["description"] ?></textarea>
                    </td>
                </tr>
                <tr>
                    <th>
                        <label>Keyword</label>
                    </th>
                    <td>
                        <input type="text" name="keyword_post" value="<?php echo @$seo_data["post"]["keyword"] ?>">
                    </td>
                </tr>
            </tbody>
        </table>
        <?php foreach($post_types as $a_posttype): ?>
            <h3>Settings for <?php echo $a_posttype ?></h3>
            <table>
                <tbody>
                    <tr>
                        <th>
                            <label>Title</label>
                        </th>
                        <td>
                            <input type="text" name="title_<?php echo $a_posttype ?>" value="<?php echo @$seo_data[$a_posttype]["title"] ?>">
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label>Description</label>
                        </th>
                        <td>
                            <textarea name="description_<?php echo $a_posttype ?>"><?php echo @$seo_data[$a_posttype]["description"] ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label>Keyword</label>
                        </th>
                        <td>
                            <input type="text" name="keyword_<?php echo $a_posttype ?>" value="<?php echo @$seo_data[$a_posttype]["keyword"] ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php 
              #Giang update for taxonomy
              $taxonomy_objects = get_object_taxonomies( $a_posttype, 'objects' ); 
              unset($taxonomy_objects['post_tag']);
              if(!empty($taxonomy_objects)){ ?>
                <div style="padding-left:50px;">
                <h3>Setting for taxonomy </h3>
                <?php foreach($taxonomy_objects as $key=>$_obj){ ?>
                 <b>- <?php echo $_obj->label.'('.$key.')';?></b>
                 <input type="hidden" name="seo_taxonomy[]" value="<?php echo $key ?>">
                <table>
                    <tbody>
                        <tr>
                            <th>
                                <label>Title</label>
                            </th>
                            <td>
                                <input type="text" placeholder="%taxonomy_name% | something... " name="tax_title_<?php echo $key ?>" value="<?php echo @$seo_data['tax'][$key]["title"] ?>">
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label>Description</label>
                            </th>
                            <td>
                                <textarea name="tax_description_<?php echo $key ?>"><?php echo @$seo_data['tax'][$key]["description"] ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label>Keyword</label>
                            </th>
                            <td>
                                <input type="text" name="tax_keyword_<?php echo $key ?>" value="<?php echo @$seo_data['tax'][$key]["keyword"] ?>">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php }?>
                </div>
              <?php } ?>
              <?php ##END : Giang update for taxonomy ?>
              
        <?php endforeach ?>
        <?php submit_button(); ?>
    </form>
</div>

<?php
}
?>
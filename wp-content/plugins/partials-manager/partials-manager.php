<?php
/*
Plugin Name: Patials Manager
Plugin URI: http://fastcoding.jp/
Description: 管理画面から共通HTMLパーツを作成し、ショートコードで呼び出すことができます。
Author: Fastcoding Mr.K
Version: 0.1
Author URI: http://fastcoding.jp/
*/

//Definition of post type "partials"
add_action( 'init', 'cptui_register_my_cpts_partials' );
function cptui_register_my_cpts_partials() {
	$labels = array(
		"name" => __( 'Partial', 'twentysixteen' ),
		"singular_name" => __( 'Partials', 'twentysixteen' ),
		);

	$args = array(
		"label" => __( 'Partial', 'twentysixteen' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "__partials", "with_front" => true ),
		"query_var" => true,
				
		"supports" => array( "title", "editor" ),				
	);
	register_post_type( "partials", $args );

// End of cptui_register_my_cpts_partials()
}

//Shorcode for displaying partial.
function partial($atts){
	extract(shortcode_atts(array(
		'name' => '',
	), $atts));

	ob_start();

	$partials = get_posts("post_type=partials&name=$name");
	if(!empty($partials)){
		$partial = $partials[0];
		echo $partial->post_content;
	}

	$output = ob_get_clean();

	return $output;
}
add_shortcode("partial", "partial");
?>
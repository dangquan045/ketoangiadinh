</div>

      <footer class="site-footer" id="footer">
        <div class="container">
          <div class="footer clearfix">
            <div class="company-info">
              <h3 class="text-uppercase">công ty tư vấn và kế toán Gia Định</h3>
              <p>Chuyên tư vấn, các thủ tục pháp lý, thánh lập hay giải thể công ty, bảo hiểm xã hội và các dịch vụ trong lĩnh vực kế toán.</p>
              <ul class="contact-info">
                <li>
                  <i class="fa fa-map-marker"></i>
                  <p>114 đường số 18, P.Hiệp Bình Chánh, Q.Thủ Đức, HCM</p>
                </li>
                <li>
                  <i class="fa fa-phone"></i>
                  <p>(+84) 038 638 519</p>
                </li>
                <li>
                  <i class="fa fa-mobile"></i>
                  <p>0987 654 321</p>
                </li>
                <li>
                  <i class="fa fa-envelope"></i>
                  <p>info@ketoangiadinh.vn</p>
                </li>
              </ul>
            </div>
            <div class="sitemap">
              <h3 class="text-uppercase">Dịch vụ chính</h3>
              <ul>
                <li><a href="#"><i class="fa fa-angle-right"></i> Thành lập công ty, doanh nghiệp</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Dịch vụ kế toán trọn gói</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Đăng ký giấy phép kinh doanh</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Dịch vụ giải thể công ty</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Bảo hiểm xã hội</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i> Dịch vụ hóa đơn</a></li>
              </ul>
            </div>
            <div class="newsletter">
              <h3 class="text-uppercase">Đăng ký nhận tin</h3>
              <div class="form-newsletter">
                <form action="./" method="post">
                  <fieldset>
                    <input type="email" name="newsemail" id="newsemail" placeholder="Nhập địa chỉ email" />
                    <input type="submit" value="Đăng ký" />
                  </fieldset>
                </form>
              </div>
              <ul class="list-inline social">
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <p class="text-center copyright">Gia Dinh accouting | © 2018. All rights reserved.</p>
      </footer>
    </div>
    <script src="<?php echo get_template_directory_uri();?>/js/bootstrap.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/slick.min.js"></script>
    <script src="<?php echo get_template_directory_uri();?>/js/scripts.js"></script>
    <?php wp_footer() ?>
  </body>
</html>
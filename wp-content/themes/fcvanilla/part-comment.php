<hr style="margin-bottom: 30px" />

<div id="comment-form">
  <div id="respond" class="rounded">
    <p class="text-center">Hãy để lại câu hỏi của bạn. chúng tôi sẽ trả lời trong thời gian sớm nhất</p>
    <form action="#" method="post" id="commentform">
      <div id="comment-email" class="form-group row">
        <label for="email" class="col-md-2 col-form-label">Email:</label>
        <div class="col-md-10"><input type="text" name="email" class="form-control" id="email" placeholder="info@gmail.com" value="" size="22" tabindex="2" aria-required="true"></div>
      </div>
      <div id="comment-phone" class="form-group row">
        <label for="phone" class="col-md-2 col-form-label">Số điện thoại:</label>
        <div class="col-md-10"><input type="text" name="phone" class="form-control" id="phone" placeholder="0987654321" value="" size="22" tabindex="3"></div>
      </div>
      <div id="comment-message" class="form-group row">
        <label for="comment" class="col-md-2 col-form-label">Nội dung:</label>
        <div class="col-md-10"><textarea name="comment" class="form-control" id="comment" cols="40" rows="4" tabindex="4"></textarea></div>
      </div>
      <div class="text-right"><input name="submit" type="submit" id="commentSubmit" tabindex="5" value="Gửi ngay"></div>
      <input type="hidden" name="comment_post_ID" value="9299" id="comment_post_ID">
      <input type="hidden" name="comment_parent" id="comment_parent" value="0">
      <input type="hidden" id="ak_js" name="ak_js" value="1523085768645">
    </form>
    <?php 
    if ( comments_open() || get_comments_number() ) :
      comments_template();
    endif;
    ?>
  </div>
</div>

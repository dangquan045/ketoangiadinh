<?php
wp_enqueue_script('jquery');
/*
 * This program is basic customization for develop & use wordpress.
 * For developer:
 * Tell me plan if you have great idea to getting better.
 * >>k-kawai@fastcoding.jp
 */

/*
 * Basic functions
 */

/*
 * is_originator
 * 
 * Discriminate one's parent page.
 * Return true if one's parent is $pagename
 * 
 * @param string: page slug
 * @param bool: include oneself
 * @param bool: check all family line
 * @return bool
 */

function is_originator($pagename, $included = true, $check_all = false) {
    if ($included && is_page($pagename)) {
        return true;
    }
    if (is_page()) { //is page?
        global $post;
        if ($post->ancestors) { //is someone's child?
         if($check_all) {
          foreach($post->ancestors as $root) {
              //$root = $post->ancestors[count($post->ancestors) - 1];
              $root_post = get_post($root);
              $name = esc_attr($root_post->post_name);
              if ($pagename == $name)
                  return true;
          }
            } else {
             $root = $post->ancestors[count($post->ancestors) - 1];
             $root_post = get_post($root);
             $name = esc_attr($root_post->post_name);
             if ($pagename == $name)
                 return true;
         }
        }
    }
    return false;
}

/*
 * Short codes
 */

/*
 * [template_uri]
 * 
 * Return template directry uri.
 * 
 * @return string
 */

function template_uri() {
    return get_template_directory_uri();
}

add_shortcode("template_uri", "template_uri");

/*
 * [url]
 * 
 * Return home url.
 * 
 * @return string
 */

function url() {
    return get_home_url();
}

add_shortcode("url", "url");
add_shortcode("home_url", "home_url");

/*
 * [site_url]
 * 
 * Return site url.
 */
add_shortcode("site_url", "site_url");

/*
 * [upload_dir]
 * 
 * Return upload directory url.
 * 
 * @return string
 */
function upload_dir() {
    $dir = wp_upload_dir();
    return $dir["basename"];
}

add_shortcode("upload_dir", "upload_dir");

/*
 * Admin page customize
 */

/*
 * Display Permalink & ID at list of pages.
 */

function add_page_columns_name($columns) {
    $columns['slug'] = "URL";
    $columns['id'] = "ID";
    return $columns;
}

function add_page_column($column_name, $post_id) {
    if ($column_name == 'slug') {
        $permalink = get_permalink($post_id);
        echo "<a href='" . $permalink . "' target='_blank'>" . $permalink . "</a>";
    }
    if ($column_name == 'id') {
        $permalink = get_permalink($post_id);
        echo "<span>$post_id</span>";
    }
}

add_filter('manage_pages_columns', 'add_page_columns_name');
add_action('manage_pages_custom_column', 'add_page_column', 10, 2);


/*
 * Clean up wp_head
 */
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_generator');

/*
 * Maintenance mode
 */

class wbConfig {

    public function __construct() {
        add_action('admin_init', Array(&$this, 'admin_init'));
    }

    public function admin_init() {
        add_filter('whitelist_options', Array(&$this, 'add_setting_field'));
        add_settings_field('maintenance', 'Maintenance Mode', Array(&$this, 'general_setting_field'), 'general');
    }

    public function add_setting_field($whitelist_options) {
        $whitelist_options['general'][] = 'maintenance';
        return $whitelist_options;
    }

    public function general_setting_field() {
        $maintenance = get_option('maintenance');
        $checked = '';
        if($maintenance === "1"){
            $checked = 'checked = "checked"';
        }
        ?>
        <input type="checkbox" name="maintenance" size="50" value="1" <?php echo $checked ?>>
        <?php
    }

}

new wbConfig();

function wpr_maintenance_mode() {
    if ((!current_user_can('edit_themes') || !is_user_logged_in()) && get_option("maintenance") === "1") {
        wp_die('ただいまメンテナンス中です。暫くお待ち下さい。');
    }
}

add_action('get_header', 'wpr_maintenance_mode');


/*
* text short
* Best way for japanese texts.
*/
function text_short($string , $l = 100){
  $string = html_entity_decode($string);
  return mb_strimwidth($string, 0, $l, "...", "utf8");
}

//Include theme style.css
function my_styles() {
    wp_enqueue_style( '', get_bloginfo( 'template_url') . '/style.css', array(), null, 'all');
}
add_action( 'wp_enqueue_scripts', 'my_styles');

/*
* Wrap division for the_content
*/

function content_wrap($the_content) {
    if(!is_page()){
        $content = '<div class="the_content">';
        $content .= $the_content;
        $content .= '</div>';
        return $content;
    }else{
        return $the_content;
    }
}
 
#add_filter('the_content','content_wrap');

/******************END DEFAULT CUSTOMIZE*********************/

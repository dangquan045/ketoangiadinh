<?php get_header() ?>
<div class="container">
<div class="slider" id="slider-top">
  <div class="slide-item"><img src="<?php echo get_template_directory_uri(); ?>/img/top/slide01.jpg" alt="Slider" /></div>
  <div class="slide-item"><img src="<?php echo get_template_directory_uri(); ?>/img/top/slide01.jpg" alt="Slider" /></div>
  <div class="slide-item"><img src="<?php echo get_template_directory_uri(); ?>/img/top/slide01.jpg" alt="Slider" /></div>
</div>

<div class="clearfix">
  <div class="content-body">
    <div class="main-service text-center">
      <div class="row">
        <?php ?>
        <div class="col-md-6">
          <div class="thumb"><a href="#"><i class="fa fa-globe"></i></a></div>
          <h2 class="post-title"><a href="#">Dịch vụ kế toán trọn gói</a></h2>
          <p class="excerpt">Hơn 10 năm kinh nghiệm, Gia Định tự tin hiểu những gì mà bạn và hơn 3500 doanh nghiệp...</p>
        </div>
        <div class="col-md-6">
          <div class="thumb"><a href="#"><i class="fa fa-building"></i></a></div>
          <h2 class="post-title"><a href="#">Thành lập công ty</a></h2>
          <p class="excerpt">Hơn 10 năm kinh nghiệm cung cấp dịch vụ THÀNH LẬP CÔNG TY/DOANH NGHIỆP. Hơn ai hết...</p>
        </div>
        <div class="col-md-6">
          <div class="thumb"><a href="#"><i class="fa fa-file-text"></i></a></div>
          <h2 class="post-title"><a href="#">Đăng ký giấy phép kinh doanh</a></h2>
          <p class="excerpt">Hơn 10 năm kinh nghiệm cung cấp dịch vụ ĐĂNG KÝ GIẤY PHÉP KINH DOANH. Hơn ai hết chúng...</p>
        </div>
        <div class="col-md-6">
          <div class="thumb"><a href="#"><i class="fa fa-recycle"></i></a></div>
          <h2 class="post-title"><a href="#">Giải thể công ty</a></h2>
          <p class="excerpt">Sau khi công ty đã có quyết định giải thể công ty, việc giải thể công ty càng sớm càng tốt, không...</p>
        </div>
        <div class="col-md-6">
          <div class="thumb"><a href="#"><i class="fa fa-home"></i></a></div>
          <h2 class="post-title"><a href="#">Dịch vụ thành lập công ty TNHH</a></h2>
          <p class="excerpt">Tổng phí dịch vụ thành lập công ty tnhh trọn gói tại ANPHA trong năm 2017 chỉ 1.200.000đ, bao...</p>
        </div>
        <div class="col-md-6">
          <div class="thumb"><a href="#"><i class="fa fa-briefcase"></i></a></div>
          <h2 class="post-title"><a href="#">Bảo hiểm xã hội</a></h2>
          <p class="excerpt">Kể từ ngày 1/12/2015 theo Quyết định 959/QĐ-BHXH ngày 09/09/2015 quy định ...</p>
        </div>
      </div>
    </div>
  </div>
  <?php get_sidebar();?>
</div>
<div class="other-service row">
  <div class="col-md-4"><a href="#">
      <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/img/top/img-thue-hoadon.jpg" alt="Thuế ban đầu và mua bán hóa đơn" />
        <figcaption>Thuế ban đầu và mua bán hóa đơn</figcaption>
      </figure>
    </a></div>
  <div class="col-md-4"><a href="#">
      <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/img/top/img-doi-tru-so.jpg" alt="Đổi trụ sở công ty" />
        <figcaption>Đổi trụ sở công ty</figcaption>
      </figure>
    </a></div>
  <div class="col-md-4"><a href="#">
      <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/img/top/img-doi-ten-cty.jpg" alt="Đổi tên công ty" />
        <figcaption>Đổi tên công ty</figcaption>
      </figure>
    </a></div>
  <div class="col-md-4"><a href="#">
      <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/img/top/img-thanh-lap-tu-nhan.jpg" alt="Dịch vụ thành lập doanh nghiệp tư nhân" />
        <figcaption>Dịch vụ thành lập doanh nghiệp tư nhân</figcaption>
      </figure>
    </a></div>
  <div class="col-md-4"><a href="#">
      <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/img/top/img-thanh-lap-co-phan.jpg" alt="Dịch vụ thành lập công ty cổ phần" />
        <figcaption>Dịch vụ thành lập công ty cổ phần</figcaption>
      </figure>
    </a></div>
  <div class="col-md-4"><a href="#">
      <figure>
        <img src="<?php echo get_template_directory_uri(); ?>/img/top/img-thanh-lap-thanh-vien.jpg" alt="Dịch vụ thành lập công ty TNHH 2 thành viên trở lên" />
        <figcaption>Dịch vụ thành lập công ty TNHH 2 thành viên trở lên</figcaption>
      </figure>
    </a></div>
</div>
</div>
<div class="map" id="map">
<div style="width:100%;height:365px;background:#ccc"></div>
</div>
<?php get_footer() ?>
<?php 
remove_filter('the_content', 'wpautop');
remove_filter('the_excerpt', 'wpautop');
?>
<?php get_header() ?>
<div class="container">
<div class="clearfix">
<div class="content-body">
  Trang tìm kiếm
  <ul>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <li>
    <a href="<?php the_permalink();?>"><h2><?php the_title();?></h2></a>
    <p><?php echo text_short(get_the_excerpt(),100);?></p>
    
  </li>
<?php endwhile; endif; ?>
</ul>
</div>
<?php get_sidebar();?>
</div>

</div>
<?php get_footer() ?>
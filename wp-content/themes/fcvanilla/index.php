<?php get_header() ?>
<div class="container">
<div class="slider" id="slider-top">
  <?php
    $images = get_field('images','option');
    $images = !empty($images) ? $images : array();
    foreach($images as $item){
  ?>
  <div class="slide-item"><img src="<?php echo $item['image']; ?>" alt="Slider" /></div>
    <?php } ?>
</div>

<div class="clearfix">
  <div class="content-body">
    <?php if ( is_active_sidebar( 'top-feature-1' ) ) : ?>
      <div id="top-feature-1">
        <?php dynamic_sidebar( 'top-feature-1' ); ?>
      </div><!-- .widget-area -->
    <?php endif; ?>

    <div class="main-service text-center">
      <div class="row">
        <?php $features = new WP_Query(
           array(
               'post_type'      => 'post',
               'meta_key'       => 'display_feature',
               'meta_value'     => '1',
               'orderby' 		=> 'publish_date',
               'order' 			=> 'ASC',
               'posts_per_page' => 6
           )
       );
       ?>
       <?php if($features->have_posts()): while($features->have_posts()): $features->the_post(); ?>
        <div class="col-md-6">
          <div class="thumb"><a href="<?php the_permalink();?>"><i class="fa <?php the_field('icon_class')?>"></i></a></div>
          <h2 class="post-title"><a href="<?php the_permalink();?>"><?php the_field('short_post_title');?></a></h2>
          <p class="excerpt"><?php echo text_short(get_the_excerpt(),100);?></p>
        </div>
        <?php endwhile;endif;?>
      </div>
    </div>
  </div>
  <?php get_sidebar();?>
</div>
<div class="other-service row">
  <?php $posts_grid = new WP_Query(
           array(
               'post_type'      => 'post',
               'meta_key'       => 'display_grid',
               'meta_value'     => '1',
               'posts_per_page' => 6,
           )
       );
       ?>
       <?php if($posts_grid->have_posts()): while($posts_grid->have_posts()): $posts_grid->the_post(); ?>
  <div class="col-md-4"><a href="<?php the_permalink();?>">
      <figure>
        <?php the_post_thumbnail('full');?>
        <figcaption><?php the_field('short_post_title');?></figcaption>
      </figure>
    </a></div><?php endwhile;endif;?>
</div>
</div>
<div class="map" id="map">
<div style="width:100%;height:365px;background:#ccc"></div>
</div>
<?php get_footer() ?>
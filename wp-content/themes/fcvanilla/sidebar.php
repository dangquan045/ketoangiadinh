<div class="content-right">
  <aside class="sidebar box-gray">
    <div class="widget-search">
      <form action="<?php echo home_url();?>" method="get"><input type="search" name="s" value="<?php echo get_search_query();?>" placeholder="Tìm kiếm..." /><button class="search-btn" type="submit"><i class="fa fa-search"></i></button></form>
    </div>
    <?php if(is_single() AND !is_single('gioi-thieu')){?>
    <div class="widget-default widget-relate-cat">
      <h3>Cùng chuyên mục</h3>
      <ul class="list-cat">
        <li><i class="fa fa-angle-right"></i><a href="#">Công ty TNHH</a></li>
        <li><i class="fa fa-angle-right"></i><a href="#">Công ty cổ phần</a></li>
        <li><i class="fa fa-angle-right"></i><a href="#">Công ty vốn nước ngoài</a></li>
        <li><i class="fa fa-angle-right"></i><a href="#">Chi nhánh công ty</a></li>
        <li><i class="fa fa-angle-right"></i><a href="#">Hộ kinh doanh cá thể</a></li>
        <li><i class="fa fa-angle-right"></i><a href="#">Công ty có điều kiện</a></li>
      </ul>
    </div>
    <?php } ?>
    <div class="widget-category">
      <ul>
        <?php $right_menu = new WP_Query(
           array(
               'post_type'      => 'post',
               'meta_key'       => 'display_menu_right',
               'meta_value'     => '1',
               'posts_per_page' => -1,
           )
       );
       ?>
       <?php if($right_menu->have_posts()): while($right_menu->have_posts()): $right_menu->the_post(); ?>
        <li><a href="<?php the_permalink();?>">
            <p><?php the_field('short_post_title');?></p>
            <small><?php the_field('menu_sum');?></small>
          </a></li><?php endwhile;endif;?>
      </ul>
    </div>
  </aside>
</div>
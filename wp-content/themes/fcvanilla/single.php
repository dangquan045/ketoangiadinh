<?php
	remove_filter('the_content', 'wpautop');
	remove_filter('the_excerpt', 'wpautop');
?>
<?php get_header() ?>
<div class="container">
<?php if(!is_single('lien-he')){?>
<div class="clearfix">
<div class="content-body">
	<?php $id = get_the_ID();?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
  <article class="post-detail">
    <h1 class="page-title"><?php the_title();?></h1>
    <?php the_content() ?>
  </article>
<?php endwhile; endif; ?>

<?php if(!is_single('gioi-thieu')){?>
      <?php
    $terms = get_the_terms(get_the_ID(),'category');
    $term = !empty($terms) ? reset($terms) : array();
    $args = array(
      'post_type' => 'post',
      'post_status' => 'publish',
      'posts_per_page'	=> 4,
      'post__not_in' => array($id),
      'tax_query' => array(
        array(
        'taxonomy' => 'category',
        'field' => 'id',
        'terms' => $term->term_id
         )
        )
      );
    $related_query = new WP_Query( $args );
  ?>
  <?php if($related_query->have_posts()): ?>
  <div class="related-post">
    <h3>Bài viết liên quan</h3>
    <ul class="list-others">
      <?php while($related_query->have_posts()): $related_query->the_post(); ?>
      <li><i class="fa fa-angle-right"></i> <a href="<?php the_permalink();?>"><?php the_title();?></a></li>
      <?php endwhile;?>
    </ul>
  </div>
  <?php endif;
  } ?>
  
  <?php get_template_part('part','comment');?>
</div>
<?php get_sidebar();?>
</div>

<?php }elseif(is_single('lien-he')){ ?>
  <?php if(have_posts()): while(have_posts()): the_post(); ?>
      <?php the_content() ?>
  <?php endwhile; endif; ?>
<?php } ?>

</div>
<?php get_footer() ?>
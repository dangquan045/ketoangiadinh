var app = app || {};

var w = $(window).width();
var h = $(window).height();

app.init = function () {

  app.slider();

  app.smoothScroll();

  app.toggleDropdown();

  app.utility();

  app.toggleLessMore();

}

/*
 * Slider top
 */
app.slider = function() {
  if($("#slider-top").length > 0) {
    $("#slider-top").slick({
      slidesToShow: 1,
      dots: true
    });
  }
};

app.smoothScroll = function() {
  $(document).on({
    "click": function(e) {
      e.preventDefault();
      var href = $(this).attr("href");
      var target = $(href === "#" || href === "" ? "html" : href);
      var position = target.offset().top;
      $("html, body").animate({
        scrollTop: position
      }, 800, "swing");
    }
  }, 'a[href^="#"]');
};

app.toggleDropdown = function() {
  // Dropdown toggle
  $('.dropdown-toggle').click(function() {
    var tg = $(this).attr('data-target');
    //$('.menu-dropdown').hide();
    $(tg).stop().toggle();
  });

  $(document).click(function(e) {
    var content = $('.menu-dropdown');
    if (!content.is(e.target) && content.has(e.target).length === 0 && $('.dropdown-toggle').has(e.target).length === 0) {
      content.hide();
    }
  });
};

app.toggleLessMore = function() {
  var toggle = $('.js-toggle-more');
  $('.toggle-more').click(function() {
    $(this).toggleClass('is-show');
    toggle.slideToggle(500);
    return false;
  });
};

app.utility = function() {
  $('.js-close-popup').click(function() {
    $(this).parents('.c-msg-popup').stop().removeClass('is-show').fadeOut('slow');
    return false;
  });
};

$(function() {

	app.init();

});
function initMap() {
  //set your google maps parameters
  var $latitude = 10.844420,
    $longitude = 106.731347,
    $map_zoom = 16;

  var icon = {
    path: 'M30.19,5.18a17.69,17.69,0,0,0-25,25L17.69,42.7,30.19,30.19A17.69,17.69,0,0,0,30.19,5.18ZM17.69,24.45a6.77,6.77,0,1,1,6.77-6.76A6.77,6.77,0,0,1,17.69,24.45Z',
    fillColor: '#009933',
    fillOpacity: 1,
    scale: 1,
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(0,35),
    labelOrigin:  new google.maps.Point(15,-10),
    strokeWeight: 0
  };

  //set google map options
  var map_options = {
    center: new google.maps.LatLng($latitude, $longitude),
    zoom: $map_zoom,
    // panControl: false,
    // streetViewControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    scrollwheel: false
  }
  //inizialize the map
  //add a custom marker to the map
  var map = new google.maps.Map(document.getElementById('map'), map_options);
  var marker = new google.maps.Marker({
    position: new google.maps.LatLng($latitude, $longitude),
    map: map,
    visible: true,
    icon: icon,
    // title: ''
    label: { color: '#009933', fontWeight: 'bold', fontSize: '14px', text: 'Tư Vấn Gia Định' }
  });
};
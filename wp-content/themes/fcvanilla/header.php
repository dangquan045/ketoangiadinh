<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="format-detection" content="telephone=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCi60FbSHBPrylts6l4Ou3pW7luUI6Vtbs&callback=initMap"
  type="text/javascript"></script>
    <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta http-equiv="cleartype" content="on">
    <![endif]-->
    <?php wp_head() ?>
    <script>
      $ = jQuery.noConflict();
    </script>
  </head>
  <body>
    <div class="wrapper">
      <header class="site-header" id="header">
        <div class="header container">
          <div class="navbar clearfix">
            <div class="navbar-brand">
              <div class="logo"><a href="<?php echo home_url();?>"><img src="<?php echo get_template_directory_uri(); ?>/img/common/logo.png" alt="accountant tax"></a></div>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-navi" aria-controls="navbar-navi" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-right float-right">
              <div class="site-info">
              	<?php if(is_front_page()): ?>
                <h1 class="sitename">Công ty kế toán và tư vấn Gia Định</h1>
                <?php else: ?>
                <p class="sitename">Công ty kế toán và tư vấn Gia Định</p>
                <?php endif; ?>
                <address><i class="fa fa-map-marker"></i>114 đường số 18, P.Hiệp Bình Chánh, Q.Thủ Đức, HCM</address>
                <div class="contact">
                  <p><i class="fa fa-phone"></i>0987 654 321</p>
                  <p><i class="fa fa-envelope"></i>info@ketoangiadinh.com</p>
                </div>
              </div>
              <ul class="social list-inline text-right">
                <li><a class="trans" href="#"><i class="fa fa-twitter"></i></a></li>
                <li><a class="trans" href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a class="trans" href="#"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="navbar-collapse navbar-expand-lg" id="navbar-navi">
          <div class="container">
            <ul class="navbar-nav">
              <li><a href="<?php echo home_url();?>/gioi-thieu.html">Giới thiệu</a></li>
              <li><a href="#">Dịch vụ thành lập</a>
                <ul class="sub-menu">
                  <li><a href="<?php echo home_url();?>/thanh-lap-cong-ty.html">Thành lập công ty</a></li>
                  <li><a href="<?php echo home_url();?>/dich-vu-thanh-lap-cong-ty-tnhh.html">Công ty TNHH</a></li>
                  <li><a href="<?php echo home_url();?>/cong-ty-co-phan.html">Công ty cổ phần</a></li>
                  <li><a href="<?php echo home_url();?>/thanh-lap-cong-ty-co-von-dau-tu-nuoc-ngoai.html">Công ty vốn nước ngoài</a></li>
                  <li><a href="<?php echo home_url();?>/dich-vu-thanh-lap-chi-nhanh-cong-ty.html">Chi nhánh công ty</a></li>
                  <li><a href="<?php echo home_url();?>/dang-ky-kinh-doanh-ho-ca-the.html">Hộ kinh doanh cá thể</a></li>
                </ul>
              </li>
              <li><a href="#">Dịch vụ kế toán</a>
                <ul class="sub-menu">
                  <li><a href="<?php echo home_url();?>/dich-vu-ke-toan-tron-goi.html">Kế toán trọn gói</a></li>
                  <li><a href="<?php echo home_url();?>/thue-ban-dau-va-mua-hoa-don.html">Khai thuế ban đầu</a></li>
                  <li><a href="<?php echo home_url();?>/lap-bao-cao-tai-chinh-cuoi-nam.html">Báo cáo tài chính</a></li>
                  <li><a href="#">Bảng phí dịch vụ</a></li>
                </ul>
              </li>
              <li><a href="#">Thay đổi GPKD</a>
                <ul class="sub-menu">
                  <li><a href="<?php echo home_url();?>/doi-ten-cong-ty.html">Thay đổi tên</a></li>
                  <li><a href="<?php echo home_url();?>/doi-tru-so-cong-ty.html">Đổi địa chỉ</a></li>
                  <li><a href="<?php echo home_url();?>/them-bot-nganh-nghe.html">Thêm ngành nghề</a></li>
                  <li><a href="<?php echo home_url();?>/tang-giam-von-dieu-le.html">Tăng vốn điều lệ</a></li>
                  <li><a href="<?php echo home_url();?>/them-giam-thanh-vien-co-dong.html">Thêm cổ đông</a></li>
                  <li><a href="<?php echo home_url();?>/doi-dai-dien-phap-luat.html">Đổi đại diện pháp luật</a></li>
                  <li><a href="<?php echo home_url();?>/chuyen-doi-loai-hinh-cong-ty.html">Đổi loại hình công ty</a></li>
                </ul>
              </li>
              <li><a href="#">Dịch vụ khác</a>
                <ul class="sub-menu">
                  <li><a href="<?php echo home_url();?>/bao-hiem-xa-hoi.html">Bảo hiểm xã hội</a></li>
                  <li><a href="<?php echo home_url();?>/giai-the-cong-ty.html">Giải thể doanh nghiệp</a></li>
                  <li><a href="<?php echo home_url();?>/dang-ky-giay-phep-kinh-doanh.html">Đăng ký kinh doanh</a></li>
                  <li><a href="<?php echo home_url();?>/dich-vu-dang-ky-thuong-hieu.html">Đăng ký nhãn hiệu,logo</a></li>
                </ul>
              </li>
              <li><a href="#">Tra cứu thông tin</a></li>
              <li><a href="<?php echo home_url();?>/lien-he.html">Liên hệ</a></li>
            </ul>
          </div>
        </div>
      </header>

      <div class="main">
        
